<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    #todo: seed pour la table pathologie et modifier la date de naissance des médecins, minimum 30 ans
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory(10)->create(['role'=>'soignant']);
        User::factory(50)->create(['role'=>'patient']);
    }
}
