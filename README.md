
## Administration & Conception de Solutions d’Infrastructure
MSPR « Administrer & sécuriser une solution d’Infrastructure à partir d’un cahier des charges »
- Contexte d'étude de cas scolaire - Groupe de 4 personnes

 **Date de livraison 27/02/22**

## A propos du projet

Intégration schema 

## Installation du projet

Cloner le projet et ouvrer le dans dans votre IDE
Lancer les 2 commandes suivantes dans votre terminal:

        composer install

        npm install

## Ouvrir un serveur local

Lancer la commande suivante dans le terminal:
        
        php artisan serve
        
## Installer la base de données

Créer une base MySql nommée "resilience" sur son serveur
Lancer la commande suivante dans le terminal:

        php artisan migrate

## Créer des données de démonstration

Lancer la commande suivante dans le terminal:
        
        php artisan db:seed --class=DatabaseSeeder

## Solution proposée
Afin de répondre a la problématique du client, notre solution a été composée de plusieurs missions réparties entre les membres du groupe : 

- **Mission 1 :** Développement de la page WEB d’authentification renforcée dont un mécanisme d’authentification à 2 facteurs

Technos choisies = Laravel + My Sql 
Pourquoi ?

<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="200"></a></p>

- **Mission 2 :** Hébergement de l’application WEB sur un serveur LINUX paramétré par votre équipe
- **Mission 3 :** Authentification via une base utilisateurs qui repose sur un annuaire Active Directory
- **Mission 4 :** Rédaction des livrables (cf section « livrables attendus ») et préparation de la soutenance

## Compétences évaluées sur ce projet
- :white_check_mark: Administrer une infrastructure.
- :white_check_mark: Gérer les accès à une infrastructure.
- :white_check_mark: Administrer la sécurité de l’infrastructure.
- :white_check_mark: Maintenir en conditions opérationnelles (Maintenance préventive et corrective).
- :white_check_mark: Tester et mettre en production des ressources afin d’améliorer une solution d’infrastructure.
- :white_check_mark: Proposer des scénarios d’évolution et d’amélioration de l’infrastructure.
- :white_check_mark: Assurer la mise en production de l’infrastructure.
- :white_check_mark: Installer, configurer et tester l’infrastructure.
- :white_check_mark: Assurer le support aux utilisateurs et aux équipes techniques.
- :white_check_mark: Gérer les demandes, les problèmes et les incidents
- :white_check_mark: Mettre à jour les référentiels de production.
- :white_check_mark: Résoudre les problèmes techniques.


## Code of Conduct

In order to ensure that the Laravel community is welcoming to all, please review and abide by the [Code of Conduct](https://laravel.com/docs/contributions#code-of-conduct).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).

## Authors

Benoît, Brice, Samuel, Sterenn
